Puppet::Functions.create_function(:hiera_consul) do

    dispatch :consul_lookup_key do
        param 'Variant[String, Numeric]', :key
        param 'Hash', :options
        param 'Puppet::LookupContext', :context
    end # dispatch end

    def consul_init (options)

	    @options = options
        host = @options['host'] || 'localhost'
        port = @options['port'] || '8500'

        Puppet.debug("[hiera-consul]: Server is #{host}:#{port}")

        @consul = Net::HTTP.new(host, port)

        @consul.read_timeout = @options['http_read_timeout'] || 10
        @consul.open_timeout = @options['http_open_timeout'] || 10

        if @options['use_ssl']
            @consul.use_ssl = true

            if @options['ssl_verify'] == false
                @consul.verify_mode = OpenSSL::SSL::VERIFY_NONE
            elsif
                @consul.verify_mode = OpenSSL::SSL::VERIFY_PEER
            end

            if @options['ssl_cert']
                store = OpenSSL::X509::Store.new
                store.add_cert(OpenSSL::X509::Certificate.new(File.read(@options['ssl_ca_cert'])))
                @consul.cert_store = store

                @consul.key = OpenSSL::PKey::RSA.new(File.read(@options['ssl_cert']))
                @consul.cert = OpenSSL::X509::Certificate.new(File.read(@options['ssl_cert']))
            end
        else
            @consul.use_ssl = false
        end

    end # consul_init end

    def consul_lookup_key(key, options, context)

        return context.cached_value(key) if context.cache_has_key(key)

        consul_init(options)
        answer = nil

        if confine_keys = options['confine_to_keys']
            raise ArgumentError, '[hiera-consul]: confine_to_keys must be an array' unless confine_keys.is_a?(Array)

            begin
                confine_keys = confine_keys.map { |r| Regexp.new(r) }
            rescue StandardError => e
                raise Puppet::DataBinding::LookupError, "[hiera-consul]: creating regexp failed with #{e}"
            end

            regex_key_match = Regexp.union(confine_keys)

            unless key[regex_key_match] == key
                Puppet.debug("[hiera-consul]: Skipping hiera-consul because key '#{key}' does not match confine_to_keys")
                context.not_found
            end
        end

        options['paths'].each do |path|


            Puppet.debug("[hiera-consul]: Lookup #{path}/#{key}")
            # Check the path is valid and won't crash hiera
            if "#{path}/#{key}".match('//')
                Puppet.debug("[hiera-consul]: The specified path #{path}/#{key} is malformed, skipping")
                next
            end
            # Only support catalog of kv store queries
            if path !~ /^\/v\d\/(catalog|kv)\//
                Puppet.debug("[hiera-consul]: Only queries to the catalog and kv store are supported not #{path}. skipping")
                next
            end

            answer = consul_get("#{path}/#{key}", options)
            next unless answer
            break
        end

        return context.not_found() unless answer
        context.cache(key, answer)

    end # lookup_key end

    def parse_result(res)
        require 'base64'
        answer = nil
        if res == 'null'
            Puppet.debug("[hiera-consul]: Jumped as consul null is not valid")
            return answer
        end
        # Consul always returns an array
        res_array = JSON.parse(res)
        # See if we are a k/v return or a catalog return
        if !res_array.empty?
            if res_array.first.include? 'Value'
                if res_array.first['Value'].nil?
                    # The value is nil so return directly without trying to decode
                    return answer
                else
                    answer = Base64.decode64(res_array.first['Value'])
                end
            else
                answer = res_array
            end
        else
            Puppet.debug("[hiera-consul]: Jumped array as empty")
        end
        answer
    end

    private

    def consul_get(path, options)
        # Token is passed only when querying kv store
        @consul['X-Consul-Token'] = options['token'] if options['token'] && path =~ /^\/v\d\/kv\//

        httpreq = Net::HTTP::Get.new(path)
        answer = nil
        begin
            result = @consul.request(httpreq)
        rescue Exception => e
            Puppet.warning('[hiera-consul]: Could not connect to Consul')
            raise Exception, e.message unless options['failure'] == 'graceful'
            return answer
        end
        unless result.is_a?(Net::HTTPSuccess)
            Puppet.debug("[hiera-consul]: HTTP response code was #{result.code}")
            return answer
        end
        Puppet.debug("[hiera-consul]: Answer was #{result.body}")
        answer = parse_result(result.body)
        answer
    end

end # Function end
